# Backup

## Backup-Ziel

fuer borgbackup zu lxc-Container auf platform.gxis.de:

- id_borgbackup ssh key (ohne Passphrase) erzeugt
- /home/root/.ssh/config mit Config fuer Zugriff (Port, ssh-key)
- Hostname fuer diese Config ist "borg-temp", Details siehe config-File
- apt install borgbackup

- der vollstaendige Pfad zum Repo muss angegeben werden, schreibbar 
  sind nur Verzeichnisse unter /home/borg/repos/

Beispiel:

```
borg init --encryption none borg@borg-temp:/home/borg/repos/testrepo

# Dann
borg create borg@borg-temp:/home/borg/repos/testrepo::test installparty.script
borg list  borg@borg-temp:/home/borg/repos/testrepo
> test                                 Mon, 2021-02-15 21:34:10 [64a0df5be5f1499004e24de82e363804f51463fe3749b498f6c377bd07d0b544]
```

(Legt "testrepo" ohne Verschluesselung an)

## Verschlüsselung

Wir verwenden den Verschlüsselungsmodus `keyfile` mit leerer Passphrase. Dass ist laut [1] möglich und empfohlen für automatisierte Backups.

Einrichtung eines Backup-Repos ist dann wie folgt möglich:

```
borg init --encryption keyfile borg@borg-temp:/home/borg/repos/testrepo
```

**Achtung** Alle entsprechenden testrepos habe ich wieder gelöscht. Man muss sich dann zum Testen ein neues Repo anlegen.

## Weiterer Plan

Wir schreiben ein Backup Script, das:
- Ein btrfs Snapshot von den relevanten Subvolumes macht
- Ein backup davon anlegt 
-- --exclude erlaubt es, Verzeichnisse auszuschließen. Zum Beispiel legt Mastodon inzwischen die Remote-Medien in einem anderen Verzeichnis ab als die von lokalen Usern. Man kann also das eine ohne das andere sichern (remote-Medien im Backup zu haben ist nicht sooo toll, wegen der Änderungsrate)
- Den snapshot wieder löscht.

[2] enthält Dokumentation, von was man Backups machen muss.

[1]: https://borgbackup.readthedocs.io/en/stable/faq.html#how-can-i-specify-the-encryption-passphrase-programmatically
[2]: https://docs.joinmastodon.org/admin/backups/

